import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

// Define IS_DEBUG for development mode
Object.defineProperty(Vue.prototype, 'IS_DEBUG', {
    value: process.env.NODE_ENV !== 'production'
});

// import global components
import './components/shared';

// import necessary plugins
import './plugins/vee-validate';

new Vue({
    render: h => h(App)
}).$mount('#app')
