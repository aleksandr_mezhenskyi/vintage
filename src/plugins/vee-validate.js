import Vue from 'vue'
import VeeValidate, {Validator} from 'vee-validate'

Vue.use(VeeValidate, {
    inject: false
});

Validator.extend('phone', {
    getMessage: (field, params, data) => (data && data.message) || 'Phone is not valid!',
    validate: value => /^(\+38)(\(\d{3}\))[\d\- ]{9}$/.test(value)
});

Validator.extend('accept', {
    getMessage: (field, params, data) => (data && data.message) || 'Accept this point!',
    validate: value => ''+value === 'true'
});